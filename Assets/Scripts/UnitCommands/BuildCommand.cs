﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BuildCommand : Command
{
    private Vector3 targetPos;
    private string placable;
	public BuildCommand(Vector3 pos,string obj){
		targetPos = pos;
        placable = obj;
    }
    public Vector3 BuildPosition(){
        return targetPos;
    }
    public string PlacableObject(){
        return placable;
    }
    public override void Execute(Unit gameobj)
    {
        gameobj.GetComponent<NavMeshAgent>().SetDestination(targetPos);
    }
}