﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackCommand : Command
{
	private Vector3 targetPos;
    private GameObject target;
	public AttackCommand(GameObject _target){
		targetPos = _target.transform.position;
        target = _target;
    }
    public GameObject GetTarget(){
        return target;
    }
    public override void Execute(Unit gameobj)
    {
        if(gameobj is Plant plant) {
            if(plant is Tomato tomato) {
                Debug.Log("Sent to tomat");
                tomato.SetTarget(target);
            }
        }
    }
}
