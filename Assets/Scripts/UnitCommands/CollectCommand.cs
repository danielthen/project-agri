﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CollectCommand : Command
{
    private GameObject target;
    private GameObject unit;
	public CollectCommand(GameObject _target){
        target = _target;
    }
    public GameObject GetTarget(){
        return target;
    }
    public override void Execute(Unit gameobj)
    {
        unit = gameobj.gameObject;
        if(unit.GetComponent<Unit>() is Farmer) {
            gameobj.GetComponent<NavMeshAgent>().SetDestination(target.transform.position);
            gameobj.StartCoroutine(CheckMove());
        }
        
    }
    IEnumerator CheckMove(){
        while(Vector3.Distance(new Vector3(unit.GetComponent<NavMeshAgent>().destination.x,unit.GetComponent<NavMeshAgent>().destination.y,0),new Vector3(target.transform.position.x,target.transform.position.y,0))<=0.5){
            if(Vector3.Distance(new Vector3(unit.transform.position.x,unit.transform.position.y,0),new Vector3(target.transform.position.x,target.transform.position.y,0))<1f){
                unit.GetComponent<Farmer>().CollectSeed(target.GetComponent<Plant>().PlantType);
                unit.GetComponent<NavMeshAgent>().SetDestination(unit.transform.position);
                GameObject.Destroy(target);
                break;
            }
            yield return null;
        }
        unit.GetComponent<NavMeshAgent>().SetDestination(unit.transform.position);
    }

}
