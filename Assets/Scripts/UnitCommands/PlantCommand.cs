﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlantCommand : Command
{
    private GameObject field;
	public PlantCommand(GameObject _target){
        field = _target;
    }
    public override void Execute(Unit gameobj)
    {
        if(gameobj is Farmer farmer) {
            gameobj.StartCoroutine(farmer.PlantSeed(field.GetComponent<FarmField>()));
        }
        
    }
}
