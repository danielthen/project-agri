﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitClickedCommand : Command {

	public Unit ClickedUnit {get; private set;}

	public UnitClickedCommand(Unit unit)
    {
		ClickedUnit = unit;
    }

	public override void Execute(Unit unit)
    {
		if(unit.GetComponentInParent<UserController>().userID == ClickedUnit.GetComponentInParent<UserController>().userID){
			if(ClickedUnit is FarmField){
				if(unit is Farmer){
					
				}else{
					//unit.ExecuteCommand(new MoveCommand(ClickedUnit.transform.position));
				}
			}
		}else{
			//unit.ExecuteCommand(new AttackCommand(ClickedUnit.gameObject));
		}
    }
}
