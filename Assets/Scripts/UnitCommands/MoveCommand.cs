﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveCommand : Command 
{
    private Vector3 movePosition {get; set;}
	private MoveGroups moveGroup;
    

	public MoveCommand(Vector3 pos, MoveGroups mg)
    {
		movePosition = pos;
		this.moveGroup = mg;
    }

	public override void Execute(Unit unit)
    {
        if (unit.GetComponent<NavMeshAgent>() != null) {
            move(unit);
			unit.moveGroups = moveGroup;
			moveGroup.AddUnit(unit);
        }
    }
	private void move(Unit unit) {
		unit.GetComponent<NavMeshAgent>().SetDestination(movePosition);
		ShowMouseClick(movePosition);
	}

	/// <summary>
	/// Shows visually mouse click in game world.
	/// Creates a gameobject that shows mouse click.
	/// </summary>
	private void ShowMouseClick(Vector3 mousePos) {
		GameObject mouseClickIndicator = (Resources.Load("Arrow") as GameObject);

		if (mouseClickIndicator != null) {
			Vector3 hitPoint = Camera.main.FindHitPoint(Input.mousePosition);
			if (!hitPoint.Equals(Vector3.positiveInfinity)) {
				GameObject mouseClick = Object.Instantiate(mouseClickIndicator, hitPoint, Quaternion.identity);
				Object.Destroy(mouseClick, 1f);
			}
		}
	}
}
