﻿
public abstract class Command
{
    public abstract void Execute(Unit gameobj);
}
