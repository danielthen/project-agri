﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableUnit : MonoBehaviour
{
    private void Start()
    {
        ShowUnitSelection();
    }

    /// <summary>
    /// Turn on or off selection indicator according to selection.
    /// </summary>
    private void ShowUnitSelection()
    {
        if (gameObject.GetComponent<MeshRenderer>() == null) {
            Debug.LogError("There must be a mesh renderer on selection indicator gameobject.");
            return;
        }
        var unit = gameObject.GetComponentInParent<Unit>();
        if (unit == null) {
            Debug.LogError("Unit selection indicator must be a child of an unit gameobject.");
            return;
        }
        unit.selectEvent += new SelectEvent(()=> { gameObject.GetComponent<MeshRenderer>().enabled = !gameObject.GetComponent<MeshRenderer>().enabled; });
        
    }
}
