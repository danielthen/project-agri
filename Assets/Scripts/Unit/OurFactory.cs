﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OurFactory : MonoBehaviour 
{
	/// <summary>
	/// Creates an object.
	/// For example use for creating plants or buildings.
	/// </summary>
	/// <param name="objectName">Object prefab name in resources folder.</param>
	/// <param name="position">Position where object should be created.</param>
	public static void CreateObject(string objectName, Vector3 position)
	{
		GameObject go = (Resources.Load (objectName) as GameObject);

		if (go == null)
		{
			Debug.LogError ("Couldn't find prefab: " + objectName + " in resources folder to create in factory.");
			return;
		}

		GameObject goClone = Instantiate (go, position, Quaternion.identity);
	}

    public static GameObject CreateObject(string objectName, Vector3 position, GameObject owner)
	{
		GameObject go = (Resources.Load (objectName) as GameObject);

		if (go == null)
		{
			Debug.LogError ("Couldn't find prefab: " + objectName + " in resources folder to create in factory.");
			return null;
		}

		GameObject goClone = Instantiate (go, position, Quaternion.identity);
		goClone.transform.parent = owner.transform;
		if(owner.GetComponent<UserController>() != null) owner.GetComponent<UserController>().AddNewUnit(goClone.GetComponent<Unit>());
		if(goClone.transform.parent.GetComponent<UserController>() != null) goClone.transform.parent.GetComponent<UserController>().AssignColors();
        if (goClone.GetComponent<Plant>() != null)
        {
			goClone.GetComponent<Plant>().Start();
		}

        return goClone;
	}
}
