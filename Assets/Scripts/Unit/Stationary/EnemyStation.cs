﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStation : Stationary
{
    public static int numberOfEnemyStations { get; set; }

    public override void Start()
    {
        health = 100;
        base.Start();
        numberOfEnemyStations += 1;
        AssignTeam(new Color(255, 0, 0));
    }

    public override void Death()
    {
        base.Death();
        numberOfEnemyStations--;
        if (numberOfEnemyStations <= 0)
        {
            Game.EndGame(Game.GameEndState.WIN);
        }
    }
}
