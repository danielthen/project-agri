﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FarmField : Stationary 
{
    // Main variables
    //[SerializeField]
    //private int maxPlantCapacity;
    public List<string> plantsGrowing;

    public override void Start()
    {
        plantsGrowing = new List<string>();
    }

    void Update()
    {
        StartGrowingPlant();
    }
    /// <summary>
    /// Starts growing a plant that is in a list of this instance.
    /// </summary>
    void StartGrowingPlant()
    {
        if (plantsGrowing.Count <= 0) return;
        string plant = plantsGrowing[0];
        plantsGrowing.RemoveAt(0);
        var plantObj = OurFactory.CreateObject(plant, gameObject.transform.position, gameObject.transform.parent.gameObject);
        plantObj.GetComponent<Plant>().changeState(Plant.PlantState.GROWING);
    }

    /// <summary>
    /// Add plant for growing.
    /// </summary>
    /// <param name="plant">Plant name.</param>
    /// <returns>True if planting was successful, otherwise false.</returns>
    public bool AddPlant(string plant)
    {
        //if (plantsGrowing.Count + 1 > maxPlantCapacity) return false;
        plantsGrowing.Add(plant);
        return true;
    }
}
