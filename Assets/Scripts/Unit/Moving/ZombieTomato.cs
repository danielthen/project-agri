﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieTomato : Plant
{
    public override void Start()
    {
        health = 30;
        aliveSprite = Resources.Load(pathStr + "zombieTomato", typeof(Sprite)) as Sprite;
        base.Start();
        PlantDamage = 10;
        PlantType = "zombieTomato";
    }
}
