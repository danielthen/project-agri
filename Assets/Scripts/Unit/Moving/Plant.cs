﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Plant : Unit
{
    public enum PlantState { SEED, GROWING, ALIVE }
    public PlantState State;//basic state for plant
    private Sprite seedSprite;
    private Sprite growSprite;
    public Sprite aliveSprite;
    public string pathStr = "Sprites/";//used for assigning sprites in script
    public string PlantType{get; set;}
    public float GrowTime { get; set; } = 10f;
    public int PlantDamage = 0;//by default the plants are harmless :P
    public float AttackSpeed{get;set;} = 1.5f;
    private float StartTime = 0;
    public bool attacking { get; set; } = false;
    private Animator anim;
    private ParticleSystem growParticles;
    private ParticleSystem damageParticles;

    public override void Start()
    {
        base.Start();
        StartTime = Time.time;
        anim = gameObject.GetComponent<Animator>();
        //Since all of the plants use the same sprites for the first 2 states,then they are assigned here
        seedSprite = Resources.Load(pathStr+"seed",typeof(Sprite)) as Sprite;
        growSprite = Resources.Load(pathStr+"growing",typeof(Sprite)) as Sprite;
        changeState(State);
        foreach (ParticleSystem addable in gameObject.GetComponentsInChildren<ParticleSystem>()){
            if(addable.transform.name == "DamageParticle"){
                damageParticles = addable;
            }
            else{
                growParticles = addable;
            }
        }
    }
    public virtual void Update(){
        switch (State) {
            case PlantState.SEED:
                break;
            case PlantState.GROWING:
                Grow();
                break;
            case PlantState.ALIVE:
                if (GetComponent<NavMeshAgent>().hasPath) { anim.SetBool("Moving", true); } 
                else { anim.SetBool("Moving", false); }
                Action();
                NavMeshCheck();
                break;
        }
    }
    
    private void NavMeshCheck() {
        var mNavMeshAgent = GetComponent<NavMeshAgent>();
        if (navMeshMoving) {
            if (mNavMeshAgent.remainingDistance < 0.4f)
                navMeshMoving = false;
            if (!mNavMeshAgent.pathPending) {
                if (mNavMeshAgent.remainingDistance <= mNavMeshAgent.stoppingDistance) {
                    if (!mNavMeshAgent.hasPath || mNavMeshAgent.velocity.sqrMagnitude == 0f) {
                        navMeshMoving = false;
                    }
                }
            }
        } else {
            if (GetComponent<NavMeshAgent>().hasPath) {
                navMeshMoving = true;
            }
        }
    }
    
    //change the state of the plant and changing sprites according to the state
    public void changeState(PlantState newState){
        State = newState;
        switch (State) {
            case PlantState.SEED:
                anim.SetBool("Moving", false);
                GetComponentInChildren<SpriteRenderer>().sprite = seedSprite;   
                break;
            case PlantState.GROWING:
                GetComponentInChildren<SpriteRenderer>().sprite = growSprite;
                anim.SetBool("Growing", true);
                break;
            case PlantState.ALIVE:
                GetComponentInChildren<SpriteRenderer>().sprite = aliveSprite;
                break;
        }
    }
    //handles the growing of the plant
    public void Grow(){
        float timePassed = Time.time - StartTime;
        if(timePassed >= 10){
            StartTime = 0;
            changeState(PlantState.ALIVE);
            anim.SetBool("Growing",false);
            growParticles.Play();
        }
        else if(timePassed >=2.5f && timePassed < 3.5f){
            growParticles.Play();
            changeState(PlantState.GROWING);
        }
    }
    public virtual void Action() {

    }
    public bool isSeed(){
        return State == PlantState.SEED;
    }
    public override void Damage(int value,GameObject damager){
        damageParticles.Play();
        attacking = true;
        base.Damage(value,damager);
    }
    public override void Death(){
        base.Death();
        changeState(PlantState.SEED);
        damageParticles.Play();
        gameObject.transform.parent = null;
        attacking=false;
        gameObject.tag = "Plant";
        gameObject.GetComponent<NavMeshAgent>().ResetPath();
        AssignTeam(new Color(255,255,255));
    }

    /// <summary>
    /// Remove this instance from enemies list in Enemy component if it is an enemy.
    /// </summary>
    private void RemoveFromEnemiesList()
    {
        if(gameObject.transform.parent.GetComponent<Enemy>() != null)
        {
            gameObject.transform.parent.GetComponent<Enemy>().units.Remove(this.gameObject);
        }
    }
}
