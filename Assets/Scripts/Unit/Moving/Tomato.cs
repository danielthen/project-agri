﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;

public class Tomato : Plant
{
    private Vector3 attackPosition = Vector3.positiveInfinity;
    private GameObject target = null;
    public float explosionRange;
    private Action<Unit> onTargetDeath;
    public ParticleSystem explosion;
    public override void Start()
    {
        health = 30;
        aliveSprite = Resources.Load(pathStr+"Tomato",typeof(Sprite)) as Sprite;
        base.Start();
        PlantDamage = 10;
        PlantType = "Tomato";
        EnemiesObserver();
    }
    /// <summary>
    /// Function, that adds listener to the enemies collection.
    /// </summary>
    private void EnemiesObserver() {
        enemies.ListChanged += new ListChangedEventHandler(
            delegate (object sender, ListChangedEventArgs e) {
                if (e.ListChangedType == ListChangedType.ItemAdded)
                    OnNewEnemyInRange();
                
            }
        );
    }
    public void SetTarget(GameObject go) {
        target = go;
        onTargetDeath = (Unit u) => { target = null; DisableCommands(); };
        target.GetComponent<Unit>().deathEvent += onTargetDeath;
        OnNewEnemyInRange();
    }
    public void SetTarget(Vector3 pos) {
        attackPosition = pos;
        OnNewEnemyInRange();
    }
    public override void Action() {
        getPath();
        if(!attackPosition.Equals(Vector3.positiveInfinity)) {
            MoveTowardsTarget(attackPosition);
        } else if(target != null) {
            MoveTowardsTarget(target.transform.position);
        }
    }
    public override void DisableCommands() {
        base.DisableCommands();
        if(target != null)
            target.GetComponent<Unit>().deathEvent -= onTargetDeath;
        target = null;
        attackPosition = Vector3.positiveInfinity;
    }
    private void MoveTowardsTarget(Vector3 dest) {
        if(GetComponent<NavMeshAgent>().destination != dest) {
            GetComponent<NavMeshAgent>().SetDestination(dest);
            Debug.Log("Tomato command to :" + dest);
        }
       
    }
    public void OnNewEnemyInRange() {
        if (target != null && enemies.Contains(target.GetComponent<Unit>())) {
            Explode();
        }
    }
    private void Explode() {
        explosion.Play();
        DisableCommands();
        for(int i = 0; i < enemies.Count; i++) {
            enemies[i].Damage(PlantDamage, gameObject);
        }
        Death();
    }
    private LineRenderer _lr;
    LineRenderer lr { get {
            if (_lr == null)
                _lr = GetComponent<LineRenderer>();
            return _lr;
        } 
        set { _lr = value; } }
    void getPath() {
        if (!GetComponent<NavMeshAgent>().hasPath)
            return;
        lr.SetPosition(0, transform.position);
        DrawPath(GetComponent<NavMeshAgent>().path);
    }
    void DrawPath(NavMeshPath path) {
        if (path.corners.Length < 2)
            return;
        lr.positionCount = path.corners.Length;
        for (var i = 1; i < path.corners.Length; i++) {
            
            lr.SetPosition(i, path.corners[i]); //go through each corner and set that to the line renderer's position
        }
    }
}
