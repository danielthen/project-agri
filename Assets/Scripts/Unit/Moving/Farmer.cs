﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System;

public class Farmer : Moving
{
    public float seedPlantDistance = 1f;
    private Vector3 placablePos;
    private bool building = false;
    public Dictionary<string,int> seedInvetory = new Dictionary<string,int>();
    
    public string farmerSeed;


    public override void Start(){
        seedInvetory.Add("Tomato",3);
        seedInvetory.Add("Pickle", 3);
        health = 200f;
        farmerSeed = null;
        base.Start();
    }
    void Update()
    {
        if(building == true){
        //Build();
        }
    }
    public override void ActivateGUI(bool value){
        GUIItemController.gui.cleanUI();
        if (value){
            AddButtonsToCanvas();
        }
    }
    private void AddButtonsToCanvas(){
        int i = 0;
        foreach(string waat in seedInvetory.Keys)
        {
            Sprite plantImage = Resources.Load("Sprites/" + waat, typeof(Sprite)) as Sprite;
            Button b = GUIItemController.gui.addButton();
            b.GetComponent<Image>().sprite = plantImage;
            b.onClick.AddListener(delegate { farmerSeed = waat; });
            i++;
        }
    }
    public void CollectSeed(string plantAsString)
    {
        if(!seedInvetory.ContainsKey(plantAsString)){
            seedInvetory.Add(plantAsString,0);
            ActivateGUI(true);
        }
        seedInvetory[plantAsString] = seedInvetory[plantAsString]+1;
    }
    public override bool allowNewCommand(){
        if(farmerSeed != null){
            return false;
        }
        return true;
    }
    public void RemoveSeed(string seed){
        if(seedInvetory[seed]==1){
            seedInvetory.Remove(seed);
            ActivateGUI(true);
            return;
        }
        seedInvetory[seed] = seedInvetory[seed]-1;
    }
    public IEnumerator PlantSeed(FarmField field) {
        var agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(field.transform.position);
        while (agent.destination.x == field.transform.position.x && agent.destination.z == field.transform.position.z) { // Kuni kasutaja ei ole muutnud teekonda
            float distFromField = Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(field.transform.position.x, 0, field.transform.position.z));
            if (distFromField < seedPlantDistance) {
                if (field.AddPlant(farmerSeed))
                    RemoveSeed(farmerSeed);
                agent.SetDestination(transform.position);
                break;
            }
            yield return null;
        }
        farmerSeed = null;
    }
}
