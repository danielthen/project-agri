﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public delegate void SelectEvent();
public abstract class Unit : MonoBehaviour 
{
	public float health;
    public float healthRestoreTime;
	private float maxHealth;
    [SerializeField]
    public BindingList<Unit> myUnits = new BindingList<Unit>();
    [SerializeField]
    public BindingList<Unit> enemies = new BindingList<Unit>();
    [SerializeField]
    public BindingList<Unit> neutrals = new BindingList<Unit>();

    public Image healthBar;
    public Command currentCommand;

    public event Action<Unit> deathEvent;
    public SelectEvent selectEvent;


    public MoveGroups moveGroups;
    public event Action<Unit> NavMeshStop;
    private bool _navmeshmove;
    public bool navMeshMoving {
        get {
            return _navmeshmove;
        }
        set {
            _navmeshmove = value;
            if (!value) {
                NavMeshStop?.Invoke(this);
            }
        }
    }
    public virtual void Start()
    {
        healthBar = gameObject.GetComponentInChildren<Canvas>().GetComponentsInChildren<Image>()[1];
        maxHealth = health;
        //gameObject.GetComponentInParent<UserController>().AddNewUnit(this);
    }
    public virtual void ActivateGUI(bool value)
    {
		
    }
    public virtual bool allowNewCommand(){
        return true;
    }
    public virtual void Damage(int value,GameObject damager)
    {
        if (maxHealth == health)
        {
           // StartCoroutine(Restore());
        }
        health -= value;
        healthBar.fillAmount = health/maxHealth;
        if(health == 0){
            Death();
        }
    }
    
    public virtual void Death()
    {
        deathEvent?.Invoke(this);
        if (GetComponent<NavMeshAgent>() != null) {
            GetComponent<NavMeshAgent>().isStopped = true;
        }
        if (gameObject.GetComponent<Farmer>() != null || gameObject.GetComponent<Farmhouse>() != null) Game.EndGame(Game.GameEndState.LOSE);
    }

    public virtual void DisableCommands() {
        moveGroups?.RemoveUnit(this);
        moveGroups = null;
        if(GetComponent<NavMeshAgent>() != null) {
            GetComponent<NavMeshAgent>().SetDestination(transform.position);
        }
    }
    public void AssignTeam(Color color){
        Renderer[] allRenderers = gameObject.GetComponentsInChildren<Renderer>();
        foreach(Renderer obj in allRenderers){
            if(obj.transform.tag == "Color"){
                obj.GetComponent<Renderer>().material.color = color;
            }
            else if(obj.transform.tag =="Color2"){
                obj.GetComponent<Renderer>().material.color = color;
            }
        }
    }
    
}
