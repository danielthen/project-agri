﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitImage : MonoBehaviour
{
    private Quaternion rotation;

    private void Awake()
    {
        rotation = transform.rotation;
    }

    void Update ()
    {
        FreezeRotation(rotation);
	}

    /// <summary>
    /// Freeze rotation.
    /// </summary>
    /// <param name="rotation"> Rotation to hold. </param>
    private void FreezeRotation(Quaternion rotation)
    {
        transform.rotation = rotation;
    }
}
