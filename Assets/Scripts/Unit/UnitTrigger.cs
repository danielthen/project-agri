﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using UnityEngine;


public class UnitTrigger : MonoBehaviour
{
    delegate void listEvent(BindingList<Unit> list, Unit u, string type);
    private Action<Unit> removeneutrEvent;
    private Action<Unit> removeenemEvent;
    private Action<Unit> removemyUnitEvent;

    private Unit u;
    public void Start() {
        u = GetComponentInParent<Unit>();
        removeneutrEvent = (Unit deadUnit) => { u.neutrals.Remove(deadUnit); };
        removeenemEvent = (Unit deadUnit) => { u.enemies.Remove(deadUnit); };
        removemyUnitEvent = (Unit deadUnit) => { u.myUnits.Remove(deadUnit); };
    }
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.GetComponent<Unit>() != null ) {
            ModifyList(AddToList, other.gameObject.GetComponent<Unit>());
        }
    }
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.GetComponent<Unit>() != null) {
            ModifyList(RemoveFromList, other.gameObject.GetComponent<Unit>());
        }
    }
    private void ModifyList(listEvent e, Unit unit) {
        if (unit.gameObject.GetComponentInParent<UserController>() == null) {
            e(u.neutrals, unit, "neutral");
        } else if (gameObject.GetComponentInParent<UserController>() == null) {
            e(u.enemies, unit, "enemy");
        } else if (unit.gameObject.GetComponentInParent<UserController>().userID != gameObject.GetComponentInParent<UserController>().userID) {
            e(u.enemies, unit, "enemy");
        } else {
            e(u.myUnits, unit, "enemy");
        }
    }
    private void AddToList(BindingList<Unit> list, Unit unit, string type) {
        switch (type) {
            case "enemy":
                unit.deathEvent += removeenemEvent;
                break;
            case "neutral":
                unit.deathEvent += removeneutrEvent;
                break;
            case "myUnit":
                unit.deathEvent += removemyUnitEvent;
                break;
        }
        list.Add(unit);
    }
    private void RemoveFromList(BindingList<Unit> list, Unit unit, string type) {
        switch (type) {
            case "enemy":
                unit.deathEvent -= removeenemEvent;
                break;
            case "neutral":
                unit.deathEvent -= removeneutrEvent;
                break;
            case "myUnit":
                unit.deathEvent -= removemyUnitEvent;
                break;
        }
        list.Remove(unit);
    }
}
