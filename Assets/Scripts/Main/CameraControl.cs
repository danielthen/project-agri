﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour 
{
	// Movement
	[SerializeField]
	private float moveSpeed;
	[SerializeField]
	private float mouseMovementStart;
    [SerializeField]
    private float maxX;
    [SerializeField]
    private float maxZ;

	// Zooming
	[SerializeField]
	private float maxCameraZoom;
	[SerializeField]
	private float minCameraZoom;
	private float currentCameraZoom;
	private GameObject miniMapIcon;
	private float defaultMiniMap = 15f;

	void Update()
	{
		MoveCamera();
		ZoomInOut ();
	}

	void Start()
	{
        if(moveSpeed <= 0) moveSpeed = 20f;
        mouseMovementStart = 10f;
        if (maxCameraZoom <= 0) maxCameraZoom = 60f;
        if (minCameraZoom <= 0) minCameraZoom = 10f;
        if (maxZ <= 0) maxZ = 90;
        if (maxX <= 0) maxX = 90;
        currentCameraZoom = 50f;
        miniMapIcon = gameObject.transform.GetComponentInChildren<Renderer>().transform.gameObject;
		miniMapIcon.transform.localScale = new Vector3(defaultMiniMap,defaultMiniMap,0);
	}

	/// <summary>
	/// For zooming the camera in and out.
	/// Changes field of view of the camera component of the object the script is attached to.
	/// </summary>
	private void ZoomInOut()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") < 0 && currentCameraZoom <= maxCameraZoom) 
		{
			currentCameraZoom += 100f * Time.deltaTime;
			defaultMiniMap += 100f*Time.deltaTime;
			miniMapIcon.transform.localScale = new Vector3(defaultMiniMap,defaultMiniMap,0);
		}
		else if (Input.GetAxis ("Mouse ScrollWheel") > 0 && currentCameraZoom >= minCameraZoom) 
		{
			currentCameraZoom -= 100f * Time.deltaTime;
			defaultMiniMap -= 100f * Time.deltaTime;
			miniMapIcon.transform.localScale = new Vector3(defaultMiniMap,defaultMiniMap,0);
		}

		if (GetComponent<Camera> () == null) 
		{
			Debug.LogError("Object: " + gameObject.ToString() + " that is not a camera has a script meant for camera.");
		}

		GetComponent<Camera> ().fieldOfView = currentCameraZoom;
	}

	/// <summary>
	/// Moves the camera.
	/// </summary>
	private void MoveCamera()
	{
		Vector3 position = transform.position;
		float verticalInput = Input.GetAxisRaw("Vertical");
		float horizontalInput = Input.GetAxisRaw ("Horizontal");

		if((verticalInput > 0 || Input.mousePosition.y >= Screen.height - mouseMovementStart) && transform.position.z <= maxZ)
		{
			position.z += moveSpeed * Time.deltaTime;
		}
		else if((verticalInput < 0 || Input.mousePosition.y < mouseMovementStart) && transform.position.z >= -maxZ)
		{
			position.z -= moveSpeed * Time.deltaTime;
		}

		if((horizontalInput > 0 || Input.mousePosition.x >= Screen.width - mouseMovementStart) && transform.position.x <= maxX)
		{
			position.x += moveSpeed * Time.deltaTime;
		}
		else if((horizontalInput < 0 || Input.mousePosition.x < mouseMovementStart) && transform.position.x >= -maxX)
		{
			position.x -= moveSpeed * Time.deltaTime;
		}
			
		transform.position = position;
	}
}
