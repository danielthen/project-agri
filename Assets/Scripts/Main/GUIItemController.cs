﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIItemController : MonoBehaviour
{
    public static GUIItemController gui;
    public Button[] buttons;
    private int buttonCount;
    private void Start() {
        buttons = new Button[transform.childCount];
        int i = 0;
        foreach(Transform t in transform){
            buttons[i] = t.GetComponent<Button>();
            i++;
        }
        buttonCount = buttons.Length;
        cleanUI();
        gui = this;
    }
    public void cleanUI() {
        for(int i = 0; i < buttonCount; i++) {
            buttons[i].gameObject.SetActive(false);
        }
        buttonCount = 0;
    }
    public Button addButton() {
        buttonCount++;
        buttons[buttonCount - 1].gameObject.SetActive(true);
        return buttons[buttonCount - 1];
    }
}
