﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Settings
{
    public static Color selectionBoxColor = new Color(0.3f, 0.8f, 0.95f, 0.25f);
    public static Color selectionBoxBorderColor = new Color(0.8f, 0.8f, 0.95f);
}
