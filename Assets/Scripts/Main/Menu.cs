﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script for changing scenes.
/// Also takes care of menu buttons.
/// </summary>
public class Menu : MonoBehaviour 
{
	/// <summary>
	/// Loads new scene.
	/// </summary>
	/// <param name="sceneIndex">Index of the scene we want to load.</param>
	public void loadScene(int sceneIndex)
	{
		SceneManager.LoadScene (sceneIndex);
	}

	/// <summary>
	/// Starts the game and unpauses it if needed.
	/// </summary>
	public void PlayButtonBehavior()
	{
        if (Time.timeScale <= 0f) Time.timeScale = 1f;
        SceneManager.LoadScene(1);
	}

	/// <summary>
	/// Exits the game.
	/// </summary>
	public void ExitButtonBehavior()
	{
        Application.Quit();
	}

    /// <summary>
    /// Pauses the game.
    /// </summary>
    public void PauseButtonBehavior()
    {
        Time.timeScale = (Time.timeScale >= 1f) ? 0f : 1f;
    }
}
