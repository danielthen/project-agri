﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    private static GameObject gameOver;
    public enum GameEndState { WIN, LOSE};
    private static bool gameOverB;

    /// <summary>
    /// Ends the game with win or lose state.
    /// </summary>
    /// <param name="state">Win or lose state.</param>
    public static void EndGame(GameEndState state)
    {
        gameOver = GameObject.FindGameObjectWithTag("GAME_OVER");

        if (gameOver == null)
        {
            Debug.LogError("Can't end the game because there isn't any game over object.");
            return;
        }

        if(gameOver.GetComponent<Text>() == null)
        {
            Debug.LogError("There isn't any Text component on game over object.");
            return;
        }

        gameOver.GetComponent<Text>().text = (state == GameEndState.WIN)? "You have won! Press any key to continue." : "You are defeated! Press any key to continue.";
        
        Time.timeScale = 0f;

        gameOverB = true;
    }

    private void Start()
    {
        gameOverB = false;
    }

    private void Update()
    {
        if (!gameOverB) return;
        if(Input.anyKey)
        {
            SceneManager.LoadScene(0);
        }
    }
}
