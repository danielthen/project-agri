﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils{
    static Texture2D _whiteTexture;
    public static Texture2D WhiteTexture
    {
        get
        {
            if (_whiteTexture == null)
            {
                _whiteTexture = new Texture2D(1, 1);
                _whiteTexture.SetPixel(0, 0, Color.white);
                _whiteTexture.Apply();
            }

            return _whiteTexture;
        }
    }
    /// <summary>
    /// Draws a border to the box.
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="thickness"></param>
    /// <param name="color"></param>
    public static void DrawScreenRectBorder(Rect rect, float thickness, Color color)
    {
        // Top
        Utils.DrawScreenRect(new Rect(rect.xMin, rect.yMin, rect.width, thickness), color);
        // Left
        Utils.DrawScreenRect(new Rect(rect.xMin, rect.yMin, thickness, rect.height), color);
        // Right
        Utils.DrawScreenRect(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), color);
        // Bottom
        Utils.DrawScreenRect(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), color);
    }
    /// <summary>
    /// Draws a box on the screen
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="color"></param>
    public static void DrawScreenRect(Rect rect, Color color)
    {
        GUI.color = color;
        GUI.DrawTexture(rect, WhiteTexture);
        GUI.color = Color.white;
    }
    /// <summary>
    /// Retuns the rect from the given coordinates.
    /// </summary>
    /// <param name="screenPosition1"></param>
    /// <param name="screenPosition2"></param>
    /// <returns>The rect of the coordinates.</returns>
    public static Rect GetScreenRect(Vector3 screenPosition1, Vector3 screenPosition2)
    {
        // Screenposition1 on hiire algusasukoht, ning screenposition2 see, kuhu sa teda hetkel liigutad

        // Viime koordinaadid yles vasakule, et lihtsam oleks aru saada
        screenPosition1.y = Screen.height - screenPosition1.y;
        screenPosition2.y = Screen.height - screenPosition2.y;
        //Arvutame nurgad
        var topLeft = Vector3.Min(screenPosition1, screenPosition2);
        var bottomRight = Vector3.Max(screenPosition1, screenPosition2);

        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }
    /// <summary>
    /// Casts a ray and checks, if there is a gameobject with a layer "Unit".
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="position"></param>
    /// <returns>Gameobject, that was hit or null, if there wasn´t a gameobject</returns>
    public static GameObject FindGameObjectAt(this Camera camera, Vector3 position) {
        // Leiab objecti, mida kaamerast üritatakse ray-ga kätte saada
        RaycastHit hit;
        LayerMask mask = 1 << LayerMask.NameToLayer("Unit");
        if (Physics.Raycast(camera.ScreenPointToRay(position), out hit, 100, mask)) {
            GameObject gameObj = hit.collider.gameObject;
            if (gameObj != null) {
                return gameObj;
            }
        }
        return null;
    }
    /// <summary>
    /// Calculates the viewport bounds on camera.
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="anchor">Start of the selectbox</param>
    /// <param name="outer">End of the selectbox</param>
    /// <returns>Bounds on camera</returns>
    public static Bounds GetViewportBounds(this Camera camera, Vector3 anchor, Vector3 outer) {
        Vector3 anchorView = camera.ScreenToViewportPoint(anchor);
        Vector3 outerView = camera.ScreenToViewportPoint(outer);
        Vector3 min = Vector3.Min(anchorView, outerView);
        Vector3 max = Vector3.Max(anchorView, outerView);

        min.z = camera.nearClipPlane;
        max.z = camera.farClipPlane;

        Bounds bounds = new Bounds();
        bounds.SetMinMax(min, max);
        return bounds;
    }
    /// <summary>
    /// Checks if the gameobject is inside of the selectionbox
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="viewportBounds"></param>
    /// <param name="position"></param>
    /// <returns>Boolean value about selection</returns>
    public static bool IsWithinBounds(this Camera camera, Bounds viewportBounds, Vector3 position) {
        Vector3 viewportPoint = camera.WorldToViewportPoint(position);
        return viewportBounds.Contains(viewportPoint);
    }
    /// <summary>
    /// Finds hitpoint of ray that is casted.
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="position"></param>
    /// <returns></returns>
	public static Vector3 FindHitPoint(this Camera camera, Vector3 position) {
        RaycastHit hit;
        if (Physics.Raycast(camera.ScreenPointToRay(position), out hit, 100)) {
            return hit.point;
        }
        return Vector3.positiveInfinity;
    }
}
