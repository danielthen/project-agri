﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(UserController))]
class SelectionLogic : MonoBehaviour {
    private bool isSelecting = false;
    private Vector3 selectionBoxStartPos;

    private UserController userController;
    private void Start() {
        userController = gameObject.GetComponent<UserController>();
    }
    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            LeftMouseClick();
        } else if (Input.GetMouseButtonUp(0)) {
            LeftMouseButtonUp();
        }
    }
    private void LeftMouseClick() {
        if (!userController.AllowSelection() || EventSystem.current.IsPointerOverGameObject())
            return;
        isSelecting = true;
        selectionBoxStartPos = Input.mousePosition; 
    }
    public void LeftMouseButtonUp() {
        if (!isSelecting)
            return;
        isSelecting = false;
        userController.DeSelectActiveUnits();
        AddUnits();
        
    }
    
    private void OnGUI() {
        if (isSelecting) {
            Rect rect = Utils.GetScreenRect(selectionBoxStartPos, Input.mousePosition);
            Utils.DrawScreenRect(rect, Settings.selectionBoxColor);
            Utils.DrawScreenRectBorder(rect, 2, Settings.selectionBoxBorderColor);
        }
    }
   
    private void AddUnits() {
        if (selectionBoxStartPos == Input.mousePosition) {
            AddSingleGameObject();
        } else {
            AddAllWithinBounds();
        }
    }
    private void AddSingleGameObject() {
        GameObject go = Camera.main.FindGameObjectAt(selectionBoxStartPos);
        if (go != null) {
            // If object doesn't have a player controlling it.
            if (go.GetComponentInParent<UserController>() == null) return;
            // If the given object isn't the parent.
            if (go.GetComponentInParent<UserController>().userID == userController.userID) {
                userController.SetActiveUnits(new List<Unit>() { go.GetComponent<Unit>() });
            }
        }
    }
    private void AddAllWithinBounds() {
        // If player created selection cube, then it adds all the player units within bounds.
        Bounds bounds = Camera.main.GetViewportBounds(selectionBoxStartPos, Input.mousePosition);
        List<Unit> select = new List<Unit>();
        for (int i = 0; i < userController.myUnits.Count; i++) {
            if (Camera.main.IsWithinBounds(bounds, userController.myUnits[i].transform.position)) {
                select.Add(userController.myUnits[i]);
            }
        }
        userController.SetActiveUnits(select);
    }
}
