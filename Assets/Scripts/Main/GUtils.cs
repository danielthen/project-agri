﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class GUtils {
	public static void RemoveChildren(this GameObject go){
		for(int i = 0; i<go.transform.childCount;i++){
			GameObject.Destroy(go.transform.GetChild(i).gameObject);
		}
	}
	public static GameObject CanvasButton(int i, Sprite img){
		GameObject go = new GameObject();
		go.AddComponent<RectTransform>();
		go.AddComponent<CanvasRenderer>();
		go.AddComponent<Image>();
		go.GetComponent<Image>().sprite = img;
		go.AddComponent<Button>();
		go.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width * TestSet.testset.width,Screen.height*TestSet.testset.height);
		go.GetComponent<RectTransform>().anchoredPosition = new Vector2(Screen.width * TestSet.testset.x + Screen.width * TestSet.testset.step * i, Screen.height*TestSet.testset.y);
		return go;
	}
}
