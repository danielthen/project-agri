﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;

public class MoveGroups
{
    public List<Unit> currentUnits = new List<Unit>();
    private Dictionary<Unit, Action<Unit>> actions = new Dictionary<Unit, Action<Unit>>();
    private Dictionary<Unit, ListChangedEventHandler> listChange = new Dictionary<Unit, ListChangedEventHandler>();
    private int stoppedUnits = 0;
    Vector3 midPoint = Vector3.zero;

    public void AddUnit(Unit plant) {
        currentUnits.Add(plant);
        midPoint = MidPoint();
        updateNavMeshDestinations();
        Action<Unit> action = (Unit unit) => { Plant_NavMeshStop(unit); };
        plant.NavMeshStop += action;
        actions.Add(plant, action);

        var list = new ListChangedEventHandler((object sender, ListChangedEventArgs e) => {
            if (e.ListChangedType == ListChangedType.ItemAdded) {
                OnCollision(plant);
            }
        });
        plant.myUnits.ListChanged += list;
        listChange.Add(plant, list);
    }
    public void RemoveUnit(Unit u) {
        if (!currentUnits.Contains(u))
            return;
        ClearUnitEvents(u);
        currentUnits.Remove(u);
        
    }
    public void OnCollision(Unit plant) {
        currentUnits.ForEach((Unit u) => {
            if (plant.myUnits.Contains(u) && !u.GetComponent<NavMeshAgent>().hasPath) {
                plant.GetComponent<NavMeshAgent>().SetDestination(plant.transform.position);
            }
        });
    }
    private void Plant_NavMeshStop(Unit plant) {
        currentUnits.ForEach((Unit u) => {
            if (plant.myUnits.Contains(u)) {
                u.GetComponent<NavMeshAgent>().SetDestination(u.transform.position);
            }
        });
        ClearUnitEvents(plant);
        stoppedUnits += 1;
        if(stoppedUnits == currentUnits.Count) {
            DestroyObj();
        }
    }
    private void ClearUnitEvents(Unit u) {
        u.NavMeshStop -= actions[u];
        u.myUnits.ListChanged -= listChange[u];
    }
    private void DestroyObj() {
        currentUnits.ForEach((Unit u) => { u.moveGroups = null; });
        currentUnits.Clear();
    }

    private Vector3 MidPoint() {
        Vector3 mid = new Vector3(0, 0, 0);
        foreach(var plant in currentUnits) {
            mid += plant.transform.position;
        }

        return mid / currentUnits.Count;
    }
    private void updateNavMeshDestinations() {
        foreach (var plant in currentUnits) {
            Vector3 deltaPos = (plant.transform.position - midPoint)/5f;
            if (!plant.GetComponent<NavMeshAgent>().pathPending) { // Kui navmeshil setdestination kutsuda, siis see võtab natuke aega (100ms). Kontroll, et sellel ajal iseenda positsiooni ei panda.
                var dest = plant.GetComponent<NavMeshAgent>().destination;
                plant.GetComponent<NavMeshAgent>().SetDestination(dest + deltaPos);
            }
        }
    }

    

}
