﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserController : MonoBehaviour 
{
    public int userID;
    public Control currentPlayer;
    public List<Unit> myUnits = new List<Unit>();
    public List<Unit> selectedUnits;
    
	[SerializeField]
    private playerType currentType;
    private enum playerType{ User, Player };
    
	public void Start()
    {
        selectedUnits = new List<Unit>();
        switch (currentType)
        {
            case playerType.User:
                currentPlayer = gameObject.AddComponent<Player>();
                break;
            case playerType.Player:
                currentPlayer = gameObject.AddComponent<Player>();
                break;
        }
        Unit[] allUnits = gameObject.GetComponentsInChildren<Unit>();
        foreach(Unit addable in allUnits){
            AddNewUnit(addable);
        }
    }
    public void objectToPlace(string obj){
          if(currentPlayer is Player){
            //Player muudetav = currentPlayer as Player;
            //muudetav.placableObj = obj;
        }
    }
    public void AssignColors(){
        if(currentPlayer is Player){
            Player muudetav = currentPlayer as Player;
            muudetav.AssignTeamColor();
        }
    }
    void Update () 
	{
        currentPlayer.ControllerUpdate();
	}
    public void AddNewUnit(Unit u)
    {
        myUnits.Add(u);

        u.deathEvent += (Unit un) => { RemoveUnit(un); };
    }
    public void RemoveUnit(Unit u)
    {
        if(selectedUnits.Remove(u))
            u.selectEvent?.Invoke();
        myUnits.Remove(u);
    }
    public bool AllowSelection() {
        if (selectedUnits.Count == 1) {
            return selectedUnits[0].allowNewCommand();
        }
        return true;
    }
    public void DeSelectActiveUnits() {
        selectedUnits.ForEach(u => u.selectEvent?.Invoke());
        if (selectedUnits.Count == 1) {
            selectedUnits[0].ActivateGUI(false);
        }
        selectedUnits.Clear();
    }
    public void SetActiveUnits(List<Unit> units) {
        selectedUnits = units;
        selectedUnits.ForEach(u => { u.selectEvent?.Invoke(); });
        if(units.Count == 1) {
            units[0].GetComponent<Unit>().ActivateGUI(true);
        }
    }
    
    public void SendCommand(Command command)
    {
        for(int i = 0; i < selectedUnits.Count; i++) {
            Unit u = selectedUnits[i];
            u.DisableCommands();
            command.Execute(u);
        }
        
    }
}
