﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Control 
{
    private UserController usercontroller;
    public Color iconColor = new Color(0,100,200,100);

	private void Start()
    {
        usercontroller = GetComponent<UserController>();
        AssignTeamColor();
        
    }
    public void AssignTeamColor(){
        Unit[] allUnits = gameObject.GetComponentsInChildren<Unit>();
       foreach (Unit change in allUnits)
       {
            change.AssignTeam(iconColor);
       }
    }
	public override void ControllerUpdate ()
    {
        if (Input.GetMouseButtonDown(1)) {
            RightMouseClick();
        }
        if (Input.GetMouseButtonUp(0)) {
            LeftMouseButtonUp();
        }
    }
    
	public void LeftMouseButtonUp()
    {
        if(!usercontroller.AllowSelection()) {
            sendCommandLeftClickSelected();
        }
    }
	public void RightMouseClick()
    {
        sendCommandRightClick();
    }
	private void sendCommandRightClick()
    {
        GameObject unit = FindGameObjectAt(Camera.main, Input.mousePosition);
        if (unit == null)
        {
            Vector3 hitPoint = Camera.main.FindHitPoint(Input.mousePosition);
            if (!hitPoint.Equals(Vector3.positiveInfinity))
            {
                usercontroller.SendCommand(new MoveCommand(hitPoint, new MoveGroups()));
            }
        }
        else
        {
            if(unit.GetComponentInChildren<Plant>() != null) 
            {
                if (unit.GetComponentInChildren<Plant>().isSeed())
                {
                    usercontroller.SendCommand(new CollectCommand(unit));
                }
                else if (unit.transform.parent != gameObject.transform)
                {
                    usercontroller.SendCommand(new AttackCommand(unit));
                }
            }
            else if (unit.transform.parent != gameObject.transform)
            {
                usercontroller.SendCommand(new AttackCommand(unit));
            }
        }
    }
    private void sendCommandRightClickSelected(){
        //Debug.Log("SendCommandRightClickSelected");
    }
    private void sendCommandLeftClickSelected(){
        if(usercontroller.selectedUnits.Count == 1){
            Unit unit = usercontroller.selectedUnits[0];
            GameObject hitObject = FindGameObjectAt(Camera.main,Input.mousePosition);
            //Vector3 hitpoint = FindHitPoint(Camera.main,Input.mousePosition);
            if(unit is Farmer){
                if(hitObject != null){
                    var field = hitObject.GetComponent<FarmField>();
                    if (field != null){
                        usercontroller.SendCommand(new PlantCommand(hitObject));
                    }
                }
            }
        }
    }
    
	
    public static GameObject FindGameObjectAt(Camera camera, Vector3 position) {
        // Leiab objecti, mida kaamerast üritatakse ray-ga kätte saada
        RaycastHit hit;
        LayerMask mask = 1 << LayerMask.NameToLayer("Unit");
        if (Physics.Raycast(camera.ScreenPointToRay(position), out hit, 100, mask)) {
            GameObject gameObj = hit.collider.gameObject;
            if (gameObj != null) {
                return gameObj;
            }
        }
        return null;
    }
    
    
    
}