﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public enum EnemyState { IDLE, ATTACK, DEFEND, BUILDING, GROWING };
    
    // Main variables.
    public List<GameObject> units { get; set; }
    [SerializeField]
    private int numberOfUnitsToCreateAndHold;
    public EnemyState state { get; set; }

    // For creating units.
    [SerializeField]
    private float creationCooldown;
    private float creationTimeStamp;
    /*[SerializeField]
    /private float minHealth;*/

    private void Awake()
    {
        units = new List<GameObject>();
        if(numberOfUnitsToCreateAndHold <= 0) numberOfUnitsToCreateAndHold = 5;
        state = EnemyState.BUILDING;
        if (creationCooldown <= 0) creationCooldown = 4f;
        /*minHealth = 50f;*/
    }
    
    private void Update()
    {
        /* Sounds good but doesn't work with this system right now.
         * // If enemy spawn point has half or less health points then send units back to regroup.
        if(gameObject.GetComponent<EnemyStation>().health <= minHealth && state != EnemyState.DEFEND)
        {
            state = EnemyState.DEFEND;
        }*/

        // If there are few enemies left then they should go back to their spawn point and regroup.
        if (units.Count <= numberOfUnitsToCreateAndHold / 2 && state != EnemyState.DEFEND && state != EnemyState.BUILDING)
        {
            state = EnemyState.DEFEND;
        }

        if (state == EnemyState.BUILDING)
        {
            // Create units after with creation cooldown.
            if (creationTimeStamp + creationCooldown >= Time.time) return;

            CreateUnit("zombieTomato");

            creationTimeStamp = Time.time;
        }
        else if (state == EnemyState.ATTACK)
        {
            GameObject target;

            target = FindRandomTarget();
            
            if (target != null) SendUnitsToAttack(target);
        }
        else if (state == EnemyState.DEFEND)
        {
            sendUnitsToLocation(transform.position);
            creationTimeStamp = Time.time;
            state = EnemyState.BUILDING;
        }
        else if (state == EnemyState.GROWING)
        {
            if (AllUnitsReadyToAttack())
                state = EnemyState.ATTACK;
        }
    }

    /// <summary>
    /// Gives all units a command to attack a target.
    /// </summary>
    /// <param name="target">Target to attack.</param>
    private void SendUnitsToAttack(GameObject target)
    {
        for (int i = 0; i < units.Count; i++)
        {
            // Nullpointer check.
            if (units[i].GetComponent<Plant>() == null)
            {
                Debug.LogError("There is an unit in enemies" + units[i].ToString() + " that isn't a plant.");
                continue;
            }

            // Give attack command to all units.
            if (units[i].GetComponent<Plant>().State == Plant.PlantState.ALIVE && !units[i].GetComponent<Plant>().attacking)
            {
                //units[i].GetComponent<Plant>().ExecuteCommand(new AttackCommand(target));
            }
        }
    }

    /// <summary>
    /// Create unit after time.
    /// </summary>
    /// <param name="unitName">Unit to create.</param>
    /// <param name="creatingTime">Unit creation time.</param>
    private void CreateUnit(string unitName)
    {
        GameObject newUnit = OurFactory.CreateObject(unitName, transform.position, gameObject);
        units.Add(newUnit);
        newUnit.GetComponentInChildren<Unit>().AssignTeam(new Color(255,0,0));
        // Switch state if there is enough units.
        if (units.Count >= numberOfUnitsToCreateAndHold)
        {
            state = EnemyState.GROWING;
        }
    }

    /// <summary>
    /// Control if all units are fully grown.
    /// </summary>
    /// <returns>True if all plants are alive, false if a plant in the list is not ready.</returns>
    private bool AllUnitsReadyToAttack()
    {
        foreach(GameObject u in units)
        {
            if (u.GetComponent<Plant>().State != Plant.PlantState.ALIVE)
                return false;
        }

        return true;
    }

    /// <summary>
    /// Send units to location.
    /// </summary>
    /// <param name="position">Location where to send units.</param>
    private void sendUnitsToLocation(Vector3 position)
    {
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i].GetComponent<Plant>() == null)
            {
                Debug.LogError("There is an unit in enemies" + units[i].ToString() + " that isn't a plant.");
                continue;
            }

            //units[i].GetComponent<Plant>().ExecuteCommand(new MoveCommand(position));
        }

        state = EnemyState.IDLE;
    }

    /// <summary>
    /// Finds a target to attack and returns it. 
    /// </summary>
    /// <returns>Target to attack.</returns>
    private GameObject FindRandomTarget()
    {
        int randomNr = UnityEngine.Random.Range(0, 2);

        // Set target
        if (randomNr == 0)
        {
            if (GameObject.FindObjectOfType<Farmhouse>() != null)
                return GameObject.FindObjectOfType<Farmhouse>().gameObject;
        }
        else if (randomNr == 1)
        {
            if (GameObject.FindObjectOfType<Farmer>() != null)
                return GameObject.FindObjectOfType<Farmer>().gameObject;
        }

        return null;
    }
}