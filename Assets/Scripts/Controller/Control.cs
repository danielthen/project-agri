﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Control:MonoBehaviour
{
    public abstract void ControllerUpdate();
}
